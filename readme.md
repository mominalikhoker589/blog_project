# AQ Tech Blog Django

[![Django](https://img.shields.io/badge/django-darkgreen?style=for-the-badge&logo=django&logoColor=white)](https://code.visualstudio.com/download)
[![Django Rest framework](https://img.shields.io/badge/djangorest-ff1709?style=for-the-badge&logo=django&logoColor=white)](https://code.visualstudio.com/download)
![PostgreSQL](https://img.shields.io/badge/PostgreSQL-316192?style=for-the-badge&logo=postgresql&logoColor=white)
[![VS Code](https://img.shields.io/badge/Visual_Studio_Code-0078D4?style=for-the-badge&logo=visual%20studio%20code&logoColor=white)](https://code.visualstudio.com/download)


## Steps to run the project

1. Clone the repository
2. Create a virtual environment using command `python3 -m venv env`
3. Install the dependencies using command `pip install -r requirements.txt`
4. Type `source env/bin/activate`
5. Run the server using command `python manage.py makemigrations`
6. Then run the server using command `python manage.py migrate`
7. Finally run the server using command `python manage.py runserver`
